from setuptools import setup, find_packages

setup(
    name="synapse-blacklist-antispam",
    version="0.0.1",
    packages=find_packages(),
    description="Simple antispam for Synapse using a blacklist from the web",
    include_package_data=True,
    zip_safe=True,
    install_requires=[],
)
