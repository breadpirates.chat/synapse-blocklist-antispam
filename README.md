# synapse-simple-antispam
A simple spam checker module for Synapse to block invites from unwanted homeservers


## Installation

In your Synapse python environment:
```bash
pip install git+https://gitlab.com/breadpirates.chat/synapse-blacklist-antispam#egg=synapse-blacklist-antispam
```

Then add to your `homeserver.yaml`:
```yaml
spam_checker:
  module: "synapse_blacklist_antispam.AntiSpamInvites"
  config:
    # A URL to fetch the blacklist from
    blacklist_url: "https://gitlab.com/breadpirates.chat/bad-homeservers/raw/master/homeservers.yaml"
    # Amount of time in minutes to wait until fetching the blacklist again
    blacklist_interval: 60
```

Synapse will need to be restarted to apply the changes. To modify the list of homeservers,
update the config and restart Synapse.
