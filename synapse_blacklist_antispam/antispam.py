from time import time
from requests import get
from yaml import load

def download_blacklist(url):
    res = get(url)
    return load(res.text)

class AntiSpamInvites(object):
    def __init__(self, config):
        self._block_invites_from = config.get("blacklist_url", "https://gitlab.com/breadpirates.chat/bad-homeservers/raw/master/homeservers.yaml")
        self._cache_interval = int(config.get("blacklist_interval", 60))
        self._last_update = time()
        self._blacklist = download_blacklist(self._block_invites_from)

    def check_event_for_spam(self, event):
        return False # not spam

    def user_may_invite(self, inviter_user_id, invitee_user_id, room_id):
        if (time() - self._last_update / 60) > self._cache_interval:
            self._blacklist = download_blacklist(self.block_invites_from)
        for bad_hs in self._blacklist['bad_homeservers']:
            if inviter_user_id.endswith(":" + bad_hs):
                return False # not allowed
        return True # allowed

    def user_may_create_room(self, user_id):
        return True # allowed

    def user_may_create_room_alias(self, user_id, room_alias):
        return True # allowed

    def user_may_publish_room(self, user_id, room_id):
        return True # allowed

    @staticmethod
    def parse_config(config):
        return config # no parsing needed
